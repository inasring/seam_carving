from pathlib import Path
from PIL import Image
import numpy as np
from scipy.ndimage import sobel
import matplotlib.pyplot as plt

images = Path("../input_imgs").glob("*")

def rgb2gray(rgb):
    r, g, b = rgb[:,:,0], rgb[:,:,1], rgb[:,:,2]
    gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
    return gray

output_img = Path("sobel_imgs/")
output_img.mkdir(parents=True, exist_ok=True)
for img_filename in images:
    img = rgb2gray(np.array(Image.open(img_filename)))
    # img_proc = sobel(rgb2gray(img))**2
    dx = sobel(img, 0)  # horizontal derivative
    dy = sobel(img, 1)  # vertical derivative
    mag = np.hypot(dx, dy)  # magnitude
    mag *= 255.0 / np.max(mag)  # normalize (Q&D)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.axis('off')
    ax.imshow(mag, cmap="gray")
    fig.savefig(output_img / ("sobel_"+img_filename.stem+".png"),
                bbox_inches="tight")
    # img_proc.save(output_img / ("sobel_"+img_filename.name))

plt.show()
using Pkg

Pkg.add("FileIO")
Pkg.add("ImageMagick")
Pkg.add("ImageIO")
Pkg.add("Images")
Pkg.add("DSP")
Pkg.add("ImageView")
Pkg.add("Gtk")
Pkg.add("Colors")

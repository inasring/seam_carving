using ImageView
using Images
using DSP
using Gtk
using FileIO


function seam_cost(image_score)
    cost_path_img = zeros(size(image_score))
    for i=1:size(image_score)[1]
        for j=1:size(image_score)[2]
            current_row = size(image_score)[1]-i+1
            current_col = size(image_score)[2]-j+1
            if i == 1
                # Copy last row
                cost_path_img[current_row,current_col] = image_score[current_row,current_col]
            else
                left_down = 1000
                right_down = 1000
                center_down = cost_path_img[current_row+1,current_col]
                if j != 1
                    right_down = cost_path_img[current_row+1,current_col+1]
                end
                if j != size(image_score)[2]
                    left_down = cost_path_img[current_row+1,current_col-1]
                end
                min_cost = min(left_down, center_down, right_down)

                cost_path_img[current_row,current_col] = image_score[current_row,current_col] + min_cost
            end
        end
    end
    return cost_path_img
end


function compute_path(cost_path_img)
    # Find column with minimum value
    minval = 1000000
    col = 1
    for j=1:size(cost_path_img)[2]
        if minval > cost_path_img[1,j]
            col = j
            minval = cost_path_img[1,j]
        end
    end
    # Compute lowest cost seam path
    # path = [(1,1) for i=1:size(cost_path_img)[1]]
    path = [i for i=1:size(cost_path_img)[1]]
    path[1] = col
    for i=1:size(cost_path_img)[1]-1
        left_down = 1000000
        right_down = 1000000
        center_down = cost_path_img[i+1, col]
        if col != 1
            left_down = cost_path_img[i+1, col-1]
        end
        if col != size(cost_path_img)[2]
            right_down = cost_path_img[i+1, col+1]
        end
        minval = min(left_down, right_down, center_down)
        if left_down == minval
            col = col-1
        elseif right_down == minval
            col = col+1
        end
        path[i+1] = col
    end
    return path
end


function carve(mat, path)
    channels, len_x, len_y = size(mat)
    ret_mat = Float32.(zeros(channels, len_x,len_y-1))
    for i=1:len_x
        if path[i] == 1
            ret_mat[:, i, :] = mat[:, i, 2:len_y]
        elseif path[i] == len_y
            ret_mat[:, i, :] = mat[:, i, 1:(len_y-1)]
        else
            to_write = mat[:, i, 1:(path[i]-1)]
            ret_mat[:, i, 1:(path[i]-1)] = to_write
            to_write = mat[:, i, (path[i]+1):len_y]
            ret_mat[:, i, path[i]:(len_y-1)] = to_write
        end
    end
    return ret_mat
end


function sobel_importance(image, image_mask=nothing)
    filter_y = Float32.([[-1 -2 -1 ]
                         [ 0  0  0 ]
                         [ 1  2  1 ]])

    filter_x = Float32.([[-1  0  1 ]
                         [-2  0  2 ]
                         [-1  0  1 ]])
    img_x = conv(image, filter_x)[2:end-1, 2:end-1]
    img_y = conv(image, filter_y)[2:end-1, 2:end-1]
    if image_mask == nothing
        return (img_x.^2 + img_y.^2).^(0.5)
    else
        return (img_x.^2 + img_y.^2).^(0.5) + 30*image_mask
    end
end


function carve_2d(img2d, path)
    # Carves a 2d tensor(matrix) instead of a 3d tensor
    # Useful to apply the same carving function to the 3d image tensor and the  
    img2d = reshape(img2d, (1, size(img2d)[end-1], size(img2d)[end]))
    img2d = carve(img2d, path)
    img2d = reshape(img2d, (size(img2d)[end-1], size(img2d)[end]))
    return img2d
end


function main(;img,
               image_mask=nothing,
               save_folder=nothing,
               show=false,
               recompute_importance_each_loop=false)

    initial_size = size(img)

    img_gray = Float32.(channelview(img))
    img_gray = 0.299*img_gray[1,:,:]+0.587*img_gray[2,:,:]+0.114*img_gray[3,:,:]
    if image_mask != nothing
        image_mask = (Float32.(channelview(image_mask)))[1,:,:]
    end
    if !recompute_importance_each_loop
        score_image = sobel_importance(img_gray, image_mask)
    end

    if show
        guidict = imshow_gui(size(img))
        c = guidict["canvas"];
        Gtk.showall(guidict["window"])
        # Create the image Signal
        imgsig = ImageView.Signal(img);
        # Show it
        imshow(c, imgsig)
    end

    for i=1:1000
        if recompute_importance_each_loop
            score_image = sobel_importance(img_gray, image_mask)
        end


        cost_path_img = seam_cost(score_image)
        path = compute_path(cost_path_img)
        img = carve(channelview(img), path)
        score_image = carve_2d(score_image, path)
        
        if image_mask!=nothing && recompute_importance_each_loop
            img_gray = carve_2d(img_gray, path)
            image_mask = carve_2d(image_mask, path)
        end


        channels, x, y = size(img)
        pad_size = initial_size[2] - y
        if pad_size != 0
            padded_img = cat(img,zeros(channels, x, pad_size), dims=3)
            display_img = colorview(RGB,padded_img)
        else
            display_img = colorview(RGB, img)
        end
        if save_folder!=nothing
            save_name = save_folder*"/img_iter_"*lpad(string(i),6,"0")*".png"
            FileIO.save(save_name, display_img)
        end
        if show
            push!(imgsig, display_img)
            sleep(0.0001)
        end
        if i%50==0
            println("i:", i, " ", size(img))
        end
    end
end

image_filename = "debate.png"
save_folder = splitext(image_filename)[1]
image = Images.load(image_filename)
image_mask = Images.load("debate_mask.png")
# main(img=image, image_mask=image_mask, save_folder=save_folder)
main(img=image, save_folder=save_folder, recompute_importance_each_loop=false)
# main(img=image, save_folder=nothing, recompute_importance_each_loop=false)
make_video () {
    ffmpeg -framerate 10 -i $1/img_iter_%06d.png  -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2" -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p output/$1.mp4
}

make_video debate

